#
#
# 3DE4.script.name:         Transfer Point Weights
#
# 3DE4.script.version:      v1.0
#
# 3DE4.script.comment:      GUI for changing point weight of multiple points,
#                           also exports / imports point weights.
#
# 3DE4.script.gui:          Object Browser::Edit
#
# 3DE4.script.hide:         false
# 3DE4.script.startup:      false
#
# 2015 Fredrik Jureen

import os, math, csv

tool_name = "Transfer Point Weights Tool"
tool_wind_w = 500
tool_wind_h = 500
cur_page = 1    # Current page
max_pnts = 20   # Maximum number of points per page
wght_dict = {}  # A dictionary containing all the points currently loaded and their data
pgrp = None

def _tpwCallback(cb_req, cb_widg, cb_act):

    global cur_page, max_pnts, wght_dict, pgrp

    if (cb_widg == 'adda') or (cb_widg == 'adds'):
        
        if cb_widg == 'adda' : cb_sel = 0     # Adds all the points of the current point group
        if cb_widg == 'adds' : cb_sel = 1     # Adds the currently selected points   
        
        pgrp      = tde4.getCurrentPGroup()
        pnt_list  = tde4.getPointList(pgrp, cb_sel)

        if not pgrp or not pnt_list:
            tde4.postQuestionRequester("Warning",
                                       "Either there are no points in the current point group " +
                                       "or none have been selected.",
                                       "Okay")
        
        else:
            pnt_name_list = []
            for pnt in pnt_list:
                pnt_name = tde4.getPointName(pgrp, pnt)
                pnt_name_list.append( (pnt, pnt_name) )
            
            pnt_name_list = sorted(pnt_name_list, key=lambda name: name[1]) # Sort by name
            
            wght_dict = {}

            # Builds a dictionary with the following structure:
            #      KEY     || LIST VALUE 0| LIST VALUE 1  | LIST VALUE 2|      LIST VALUE 3        |
            # _____________||_____________|_______________|_____________|__________________________|
            # <Point Name> :   <Point ID> | <Point Weight>| <New Weight>| <Apply New Weight Toggle>|
            for pnt_name in pnt_name_list:
                pnt = pnt_name[0]
                name = pnt_name[1]
                wght_dict[name] = [pnt, tde4.getPointWeight(pgrp, pnt), 1.0, 0]
            
            cur_page = 1

            _savePage(cb_req)
            _updateWeightListUI(cb_req)
            _createPageNavigationUI(cb_req)
            _createTransferControlsUI(cb_req)
    
    elif cb_widg == 'prev':
        if cur_page > 1:
            cur_page -= 1

            _savePage(cb_req)
            _updateWeightListUI(cb_req)
            _createPageNavigationUI(cb_req)
    elif cb_widg == 'next':
        cb_last_page = math.trunc(len(wght_dict) / max_pnts) + 1
        if cur_page < cb_last_page:
            cur_page += 1

            _savePage(cb_req)
            _updateWeightListUI(cb_req)
            _createPageNavigationUI(cb_req)
            
    elif cb_widg == 'tggl':
        _togglePoints(cb_req)
        _updateWeightListUI(cb_req)
        
    elif cb_widg == 'tran':
        _savePage(cb_req)
        _transferWeights(cb_req)
    
    elif cb_widg == 'exp':
        _exportWeights(cb_req)
    
    elif cb_widg == 'imp':
        _importWeights(cb_req)
        _updateWeightListUI(cb_req)
    
    tde4.postCustomRequesterAndContinue(req, tool_name, tool_wind_w, tool_wind_h)
    
def _updateWeightListUI(wl_req):
    
    global cur_page, max_pnts, wght_dict
    
    wl_end = cur_page * max_pnts
    wl_strt = cur_page * max_pnts - max_pnts

    pnt_list = list(enumerate(wght_dict.keys()))
    
    # Loops through the max value of entries and either adds or updates
    for pnt in range(0, max_pnts):
        
        no_more_pnts = True         # Assumed to be true
        
        pnt_numb = 0                # Default values
        pnt_name = ""               #  |
        pnt_labl = ""               #  V
        new_wght_labl = ""
        cur_wght_val = ""
        new_wght_val = ""
        tgl_wght_val = ""
        tgl_wght_labl = ""
        
        # Prepares widget names
        sep_name = "sep" + str(pnt)
        cur_wght_name = "cur_wght" + str(pnt)
        new_wght_name = "new_wght" + str(pnt)
        tgl_wght_name = "tgl_wght" + str(pnt)
        
        # If there's a point for the current iteration
        #  -> Prepares values for the corresponding widgets
        if (wl_strt + pnt) < len(pnt_list):
            no_more_pnts = False
            pnt_numb, pnt_name = pnt_list[wl_strt + pnt]
            pnt_labl = pnt_name
            new_wght_labl = "New Weight: "
            cur_wght_val = str(wght_dict[pnt_name][1])
            new_wght_val = str(wght_dict[pnt_name][2])
            tgl_wght_val = str(wght_dict[pnt_name][3])
            tgl_wght_labl = "Transfer:"

        # If there are no widgets for the current entry,
        #  -> Creates the corresponding widgets
        if tde4.widgetExists(wl_req, cur_wght_name) == 0:
            tde4.addSeparatorWidget(wl_req, sep_name)
            tde4.setWidgetOffsets(wl_req, sep_name, 0, 0, -5, 0)
            
            tde4.addTextFieldWidget(wl_req, cur_wght_name, pnt_labl)
            tde4.setWidgetSensitiveFlag(wl_req, cur_wght_name, 0)
            tde4.setWidgetLinks(wl_req, cur_wght_name, sep_name, "", sep_name, "")
            tde4.setWidgetAttachModes(wl_req, cur_wght_name, "ATTACH_AS_IS","ATTACH_NONE", "ATTACH_AS_IS", "ATTACH_AS_IS")
            tde4.setWidgetSize(wl_req, cur_wght_name, 30, 20)
            
            tde4.addTextFieldWidget(wl_req, new_wght_name, new_wght_labl)
            tde4.setWidgetLinks(wl_req, new_wght_name, cur_wght_name, "", cur_wght_name, "")
            tde4.setWidgetAttachModes(wl_req,new_wght_name,"ATTACH_WIDGET", "ATTACH_NONE", "ATTACH_OPPOSITE_WIDGET", "ATTACH_NONE")
            tde4.setWidgetSize(wl_req, new_wght_name, 30, 20)
            
            tde4.addToggleWidget(wl_req, tgl_wght_name, tgl_wght_labl, 1)
            tde4.setWidgetLinks(wl_req, tgl_wght_name, cur_wght_name, "", cur_wght_name, "")
            tde4.setWidgetAttachModes(wl_req,tgl_wght_name,"ATTACH_WIDGET", "ATTACH_NONE", "ATTACH_OPPOSITE_WIDGET", "ATTACH_NONE")

        # If there's no point specified for the current entry
        #  -> Moves the widgets out of view
        if no_more_pnts:
            tde4.setWidgetOffsets(wl_req, cur_wght_name, 1000, 0, -2, 0)
            tde4.setWidgetOffsets(wl_req, new_wght_name, 1000, 0, 0, 0)
            tde4.setWidgetOffsets(wl_req, tgl_wght_name, 1000, 0, 0, 0)
    
        # If there is an entry
        #  -> Moves them into view
        else:
            tde4.setWidgetOffsets(wl_req, cur_wght_name, 40, 0, -2, 0)
            tde4.setWidgetOffsets(wl_req, new_wght_name, 90, 0, 0, 0)
            tde4.setWidgetOffsets(wl_req, tgl_wght_name, 220, 0, 0, 0)

        # Updates widget values
        tde4.setWidgetLabel(wl_req, new_wght_name, new_wght_labl)
        tde4.setWidgetValue(wl_req, new_wght_name, new_wght_val)
        tde4.setWidgetLabel(wl_req, cur_wght_name, pnt_labl)
        tde4.setWidgetValue(wl_req, cur_wght_name, cur_wght_val)
        tde4.setWidgetValue(wl_req, tgl_wght_name, tgl_wght_val)
        tde4.setWidgetLabel(wl_req, tgl_wght_name, tgl_wght_labl)
    
def _savePage(sp_req):

    global cur_page, max_pnts, wght_dict

    # Loops through the current widgets and saves their values to the list
    for pnt in range(0, max_pnts):
        cur_wght_name = "cur_wght" + str(pnt)
        tgl_wght_name = "tgl_wght" + str(pnt)
        if (tde4.widgetExists(sp_req, cur_wght_name) == 1) and (tde4.widgetExists(sp_req, tgl_wght_name) == 1) :
            pnt_name = tde4.getWidgetLabel(sp_req, cur_wght_name)
            if (pnt_name != "") and (pnt_name in wght_dict):
                new_wght_name = "new_wght" + str(pnt)
                widg_wght = tde4.getWidgetValue(sp_req, new_wght_name)
                widg_tgl = tde4.getWidgetValue(sp_req, tgl_wght_name)
                pnt_id, cur_wght, new_wght, pnt_tgl = wght_dict[pnt_name]
                wght_dict[pnt_name] = [pnt_id, cur_wght, widg_wght, widg_tgl]
                

def _exportWeights(ew_req):
    
    global wght_dict

    file = tde4.postFileRequester("Export Point Weights", '*.txt', '')

    text_file = open(file, "w")

    pnt_numb = 0
    
    if not text_file.closed:   
        columns = ["Point Name", "Point Weight"]
        rows = [] 
        for pnt in wght_dict:
            pnt_data = wght_dict[pnt]
            rows.append([pnt, pnt_data[1]])
            pnt_numb +=1
            
        file_write = csv.writer(text_file)
        file_write.writerow(columns)
        file_write.writerows(rows)
        
        text_file.close()
    
    if pnt_numb > 0:
        tde4.postQuestionRequester("Point weight export",
                                   "%i point weights were exported to %s" % (pnt_numb, file),
                                   "Okay")

def _importWeights(iw_req):
    
    global wght_dict
    
    file = tde4.postFileRequester("Export Point Weights", '*.txt', '')

    text_file = open(file, "r")

    pnt_numb = 0
    
    if not text_file.closed:
        file_read = csv.reader(text_file)
        headers = next(file_read)

        for row in file_read:

            if row[0] in wght_dict:
                pnt_id, cur_wght, new_wght, pnt_tgl = wght_dict[row[0]]
                wght_dict[row[0]] = [pnt_id, cur_wght, row[1], 1]
                pnt_numb += 1
        
        text_file.close()

    if pnt_numb > 0:
        tde4.postQuestionRequester("Point weight import",
                                   "%i point weights were imported from %s" % (pnt_numb, file),
                                   "Okay")
                

def _togglePoints(tp_req):

    global cur_page, max_pnts, wght_dict

    # Loops through the current widgets and sets the toggle to the opposite value of the first entry
    for pnt in range(0, max_pnts):
        cur_wght_name = "cur_wght" + str(pnt)
        tgl_wght_name = "tgl_wght" + str(pnt)
        if (tde4.widgetExists(tp_req, cur_wght_name) == 1) and (tde4.widgetExists(tp_req, tgl_wght_name) == 1) :
            pnt_name = tde4.getWidgetLabel(tp_req, cur_wght_name)
            if (pnt_name != "") and (pnt_name in wght_dict):
                if pnt == 0:
                    tgl_base = (tde4.getWidgetValue(tp_req, tgl_wght_name) -1) * -1 # Toggle boolean
                widg_tgl = tgl_base
                pnt_id, cur_wght, new_wght, pnt_tgl = wght_dict[pnt_name]
                wght_dict[pnt_name] = [pnt_id, cur_wght, new_wght, widg_tgl]


def _createPageNavigationUI(pn_req):
    
    global tool_name, tool_wind_w, tool_wind_h, cur_page, max_pnts, wght_dict

    pn_itms = len(wght_dict)
    pn_strt = cur_page * max_pnts - max_pnts
    pn_end = cur_page * max_pnts

    if (pn_itms > 0) and (pn_itms < pn_end):
        pn_end = pn_itms

    cur_pnts_labl = "Displaying points "
    cur_pnts_val = "%i - %i" % (pn_strt + 1, pn_end)
    max_pnts_labl = "of "
    max_pnts_val = str(pn_itms)
    prev_labl = "<<"
    next_labl = ">>"
    zero_pnts_labl = "No points have been added."

    if tde4.widgetExists(pn_req, 'pn_sep') == 0:
        tde4.addSeparatorWidget(pn_req, 'pn_sep')
        tde4.setWidgetOffsets(pn_req, 'pn_sep', 0, 0, -5, 0)

    # If there are no points
    #  -> Displays 'no points' message
    if pn_itms == 0:

        tde4.addTextFieldWidget(pn_req, 'zero_pnts', zero_pnts_labl)
        tde4.setWidgetSensitiveFlag(pn_req, 'zero_pnts', 0)
        
        tde4.setWidgetOffsets(pn_req, 'zero_pnts', 60, 0, 5, 0)
        tde4.setWidgetLinks(pn_req, 'zero_pnts', 'pn_sep', '', 'pn_sep', '')
        tde4.setWidgetAttachModes(pn_req, 'zero_pnts', "ATTACH_AS_IS", "ATTACH_NONE", "ATTACH_AS_IS", "ATTACH_AS_IS")
        tde4.setWidgetSize(pn_req, 'zero_pnts', 0, 20) # Hides the text box

    
    # If there are points
    #  -> Adds page navigation controls
    else:

        if tde4.widgetExists(pn_req, 'zero_pnts') == 1:
            tde4.removeWidget(pn_req, 'zero_pnts')

        if tde4.widgetExists(pn_req, 'cur_pnts') == 0:
            tde4.addTextFieldWidget(pn_req, 'cur_pnts', cur_pnts_labl)
    
            tde4.setWidgetSensitiveFlag(pn_req, 'cur_pnts', 0)

            tde4.setWidgetOffsets(pn_req, 'cur_pnts', 30, 0, 5, 0)
            tde4.setWidgetLinks(pn_req, 'cur_pnts', 'pn_sep', "", 'pn_sep', "")
            tde4.setWidgetAttachModes(pn_req, 'cur_pnts', "ATTACH_AS_IS", "ATTACH_NONE", "ATTACH_AS_IS", "ATTACH_AS_IS")
            tde4.setWidgetSize(pn_req, 'cur_pnts', 30, 20)
    
    
        tde4.setWidgetLinks(pn_req, 'cur_pnts', 'pn_sep', "", 'pn_sep', "")
        tde4.setWidgetValue(pn_req, 'cur_pnts', cur_pnts_val)

        if tde4.widgetExists(pn_req, 'max_pnts') == 0:
            tde4.addTextFieldWidget(pn_req, 'max_pnts', max_pnts_labl)

            tde4.setWidgetSensitiveFlag(pn_req, 'max_pnts', 0)
        
            tde4.setWidgetOffsets(pn_req, 'max_pnts', 30, 0, 0, 0)
            tde4.setWidgetLinks(pn_req, 'max_pnts', 'cur_pnts', "", 'cur_pnts', "")
            tde4.setWidgetAttachModes(pn_req, 'max_pnts', "ATTACH_WIDGET", "ATTACH_NONE", "ATTACH_OPPOSITE_WIDGET", "ATTACH_NONE")
            tde4.setWidgetSize(pn_req, 'max_pnts', 30, 20)

    
        tde4.setWidgetLinks(pn_req, 'max_pnts', 'cur_pnts', "", 'cur_pnts', "")
        tde4.setWidgetValue(pn_req, 'max_pnts', max_pnts_val)
    
        if tde4.widgetExists(pn_req, 'prev') == 0:
            tde4.addButtonWidget(pn_req, 'prev', prev_labl, 80, 155)
            tde4.setWidgetCallbackFunction(pn_req, 'prev', "_tpwCallback")
        
            tde4.setWidgetOffsets(pn_req, 'prev', 10, 0, 0, 0)
            tde4.setWidgetLinks(pn_req, 'prev', 'max_pnts', "", 'max_pnts', "")
            tde4.setWidgetAttachModes(pn_req, 'prev', "ATTACH_WIDGET", "ATTACH_NONE", "ATTACH_OPPOSITE_WIDGET", "ATTACH_NONE")

        tde4.setWidgetLinks(pn_req, 'prev', 'max_pnts', "", 'max_pnts', "")
        
        if tde4.widgetExists(pn_req, 'next') == 0:
            tde4.addButtonWidget(pn_req, 'next', next_labl, 80, 155)
            tde4.setWidgetCallbackFunction(pn_req, 'next', "_tpwCallback")
            tde4.setWidgetOffsets(pn_req, 'next', 10, 0, 0, 0)
            tde4.setWidgetLinks(pn_req, 'next', 'prev', "", 'prev', "")
            tde4.setWidgetAttachModes(pn_req, 'next', "ATTACH_WIDGET", "ATTACH_NONE", "ATTACH_OPPOSITE_WIDGET", "ATTACH_NONE")
        tde4.setWidgetLinks(pn_req, 'next', 'prev', "", 'prev', "")

def _createTransferControlsUI(tc_req):
    
    # Creates the transfer control buttons unless they aready exist
    
    imp_xist = None
    exp_xist = None
    tran_xist = None
    
    try: imp_xist = tde4.widgetExists(tc_req, 'imp')
    except (ValueError): None
    
    if not imp_xist:
        tde4.addButtonWidget(req,"imp", "Import Point Weights", 80, 300)
        tde4.setWidgetOffsets(req, "imp", 300, 0, 20, 0)
        tde4.setWidgetCallbackFunction(tc_req, "imp", "_tpwCallback")
        
    try: exp_xist = tde4.widgetExists(tc_req, 'exp')
    except (ValueError): None
    
    if not exp_xist:
        tde4.addButtonWidget(req,"exp", "Export Point Weights", 80, 300)
        tde4.setWidgetCallbackFunction(req, "exp", "_tpwCallback")

    try: tran_xist = tde4.widgetExists(tc_req, 'tran')
    except (ValueError): None
    
    if not tran_xist:
        tde4.addButtonWidget(req,"tran", "Transfer", 200, 200)
        tde4.setWidgetSize(req, 'tran', 70, 50)
        tde4.setWidgetOffsets(req, "tran", 400, 0, -30, 0)
        tde4.setWidgetCallbackFunction(tc_req, "tran", "_tpwCallback")

def _transferWeights(tw_req):
     
     global wght_dict, pgrp
 
     tran_numb = 0
     tran_max = len(wght_dict)
 
     if not tran_max:
         tde4.postQuestionRequester("Warning",
                                    "No points have been added",
                                    "Okay")
 
     for pnt in wght_dict:
     
         tran_pass = False # Transfer is assumed to not pass
         
         pnt_id, cur_wght, new_wght, pnt_tgl = wght_dict[pnt]
         
         new_wght = float(new_wght)
         
         if pnt_tgl:
             if new_wght != cur_wght:
                 try:
                     tde4.setPointWeight(pgrp, pnt_id, new_wght)
                     tran_pass = True
                 except (ValueError,NameError,TypeError):
                     tran_pass = False
    
         if tran_pass:
             wght_dict[pnt] = [pnt_id, new_wght, new_wght, 0]
             tran_numb += 1 # If transfer passed adds to counter
                 
     tde4.postQuestionRequester("Transfer completed",
                                "%i of %i point weights were transferred successfully." % (tran_numb, tran_max),
                                "Okay")

     _updateWeightListUI(tw_req)
        

# Creates a new window unless there's an old one
try:
    req = _tpw_req
except (ValueError,NameError,TypeError):
    req = tde4.createCustomRequester()
    _tpw_req = req

    # Widgets
    tde4.addButtonWidget(req,"adda", "Add all points", 80, 155)
    tde4.addButtonWidget(req,"adds", "Add selected points", 80, 155)
    tde4.setWidgetOffsets(req, "adds", 250, 0, -13, 0)
    tde4.setWidgetLinks(req, "adds", "adda", "", "adda", "")
    tde4.setWidgetAttachModes(req, "adds", "ATTACH_AS_IS", "ATTACH_NONE", "ATTACH_AS_IS", "ATTACH_AS_IS")
    tde4.addButtonWidget(req,"tggl", "Toggle points", 80, 210)
    tde4.setWidgetOffsets(req, "tggl", 400, 0, -13, 0)
    tde4.setWidgetLinks(req, "tggl", "adda", "", "adda", "")
    tde4.setWidgetAttachModes(req, "tggl", "ATTACH_AS_IS", "ATTACH_NONE", "ATTACH_AS_IS", "ATTACH_AS_IS")
    
    _updateWeightListUI(req)
    _createPageNavigationUI(req)
    
    tde4.setWidgetCallbackFunction(req, "adda", "_tpwCallback")
    tde4.setWidgetCallbackFunction(req, "adds", "_tpwCallback")
    tde4.setWidgetCallbackFunction(req, "tggl", "_tpwCallback")

tde4.postCustomRequesterAndContinue(req, tool_name, tool_wind_w, tool_wind_h)
