# 2015 Fredrik Jureen
#
# 3DE4.script.name:	File path Search and Replace
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Search and replace the path of loaded images.
#
# 3DE4.script.gui:	Object Browser::Edit
#
# 3DE4.script.hide: false
# 3DE4.script.startup: false

# Main callback function
def _snrCallback(cb_req, cb_widg, cb_act):

    cb_srch_f = tde4.getWidgetValue(cb_req,"srch_f")
    if cb_srch_f is None: cb_srch_f = ''
    cb_repl_w = tde4.getWidgetValue(cb_req,"repl_w")
    if cb_repl_w is None: cb_repl_w = ''
    cb_cams   = tde4.getWidgetValue(cb_req,"tgle_cams")
    cb_cams   = (cb_cams - 1) * -1 # Flips boolean to play nice with getCameraList
    cams_list = tde4.getCameraList(cb_cams)

    if not cams_list :
        warn = tde4.postQuestionRequester("Warning", "Either this project has no cameras or none have been selected", "Okay")

    else:
        for cam in cams_list:
            old_path = tde4.getCameraPath(cam)
            new_path = old_path.replace(cb_srch_f, cb_repl_w)
            tde4.setCameraPath(cam, new_path)

    return

# Creates a new window unless there's an old one
try:
    req = _snr_req
except (ValueError,NameError,TypeError):
    req = tde4.createCustomRequester()
    _snr_req = req

    # Widgets
    tde4.addTextFieldWidget(req, "srch_f", "Search for: ", "")
    tde4.addTextFieldWidget(req, "repl_w", "Replace with: ", "")
    tde4.addToggleWidget(req, "tgle_cams", "Perform on ALL cameras: ", 0)
    tde4.addButtonWidget(req,"repl", "Replace", 80, 155)
    tde4.setWidgetCallbackFunction(req, "repl", "_snrCallback")

tde4.postCustomRequesterAndContinue(req, "Search and Replace file paths", 500, 0)
