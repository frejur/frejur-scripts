#
#
# 3DE4.script.name:         Export 2d Tracks of Multiple Cameras
#
# 3DE4.script.version:      v1.0
#
# 3DE4.script.comment:      Exports the 2d tracks of the selected cameras (And of the current point group) into multiple txt files.
#
# 3DE4.script.gui:          Main Window::3DE4::File::Export
# 3DE4.script.gui:          Object Browser::Edit
#
# 3DE4.script.hide:         false
# 3DE4.script.startup:      false
#
# 2015 Fredrik Jureen

import re, os

# Main callback function
def _xmcCallback(cb_req, cb_widg, cb_act):

    cb_pref   = tde4.getWidgetValue(cb_req, "pref")
    if cb_pref is None: cb_pref = ''
    cb_odir   = tde4.getWidgetValue(cb_req, "odir")
    pgrp      = tde4.getCurrentPGroup()
    pnt_list  = tde4.getPointList(pgrp,0)
    cam_list  = tde4.getCameraList(1)

    if not pgrp or not pnt_list:
        tde4.postQuestionRequester("Warning",
                                   "Either there is no current point group or it is empty.",
                                   "Okay")

    elif len(cam_list) < 2:
        tde4.postQuestionRequester("Warning",
                                   "This script will run on whatever cameras are selected, so please select at least two them!",
                                   "Okay")

    elif not re.match('^[\w\.]+$', cb_pref):
        tde4.postQuestionRequester("Warning",
                                   "The prefix can only contain alphanumeric characters, hyphens, dots and underscores.",
                                   "Okay")

    elif not cb_odir or not os.path.isdir(cb_odir):
        tde4.postQuestionRequester("Warning",
                                   "Please select a valid output directory!",
                                   "Okay")

    else:
        cb_offs   = int(tde4.getWidgetValue(cb_req,"offs"))
        if cb_offs < 0: cb_offs = 0

        i = 0
        cam_padd = len(str(len(cam_list)))

        for cam in cam_list:
            # Code stolen from export_tracks v.1.4 (And modified)
            i += 1

            fram_numb = tde4.getCameraNoFrames(cam)
            imag_wdth = tde4.getCameraImageWidth(cam)
            imag_hght = tde4.getCameraImageHeight(cam)
            file_name = cb_odir + cb_pref + str(i).zfill(cam_padd) + '.txt'

            text_file = open(file_name, "w")
            if not text_file.closed:
                text_file.write("%d\n" % (len(pnt_list)))

                for pnt in pnt_list:
                    pnt_name = tde4.getPointName(pgrp, pnt)
                    text_file.write(pnt_name); text_file.write("\n")
                    pnt_colr = tde4.getPointColor2D(pgrp, pnt)
                    text_file.write("%d\n" % (pnt_colr))
                    pnt_data = tde4.getPointPosition2DBlock(pgrp, pnt, cam, 1, fram_numb)

                    n = 0
                    for data in pnt_data:

                        if data[0] != -1.0 and data[1] != -1.0:
                            n += 1

                    text_file.write("%d\n" % (n))
                    fram = 1 + cb_offs

                    for data in pnt_data:

                        if data[0] != -1.0 and data[1] != -1.0:
                            text_file.write("%d %.15f %.15f\n" % (fram, data[0] * imag_wdth, data[1] * imag_hght))
                            fram += 1

                text_file.close()

            else:
                tde4.postQuestionRequester("Export 2d Tracks of Multiple Cameras",
                                           "Error, couldn't open file.",
                                           "Ok")
    return

# Creates a new window unless there's an old one
try:
    req = _xmc_req
except (ValueError,NameError,TypeError):
    req = tde4.createCustomRequester()
    _xmc_req = req

    # Widgets
    tde4.addTextFieldWidget(req, "pref", "Filename prefix: ", "")
    tde4.addFileWidget(req,"odir", "Output directory: ", "*")
    tde4.addTextFieldWidget(req, "offs", "Frame offset: ", "0")
    tde4.addButtonWidget(req,"xprt", "Export", 80, 155)
    tde4.setWidgetCallbackFunction(req, "xprt", "_xmcCallback")

tde4.postCustomRequesterAndContinue(req, "Export the 2d tracks of the selected cameras", 500, 0)
