#
#
# 3DE4.script.name:         Import 2d Tracks for Multiple Cameras
#
# 3DE4.script.version:      v1.0
#
# 3DE4.script.comment:      Imports 2d tracks from all txt files found in the given directory.
# 3DE4.script.comment:      The points are added to the current point group of the selected cameras in a sequential manner.
# 3DE4.script.comment:      Points with identical names are replaced in order to link them across multiple ref images.
# 3DE4.script.comment:      The names of the text files determine the order in which they are applied to the cameras.
#
# 3DE4.script.gui:          Main Window::3DE4::File::Export
# 3DE4.script.gui:          Object Browser::Edit
#
# 3DE4.script.hide:         false
# 3DE4.script.startup:      false
#
# 2015 Fredrik Jureen

import os, fnmatch

# Main callback function
def _imcCallback(cb_req, cb_widg, cb_act):

    cb_idir   = tde4.getWidgetValue(cb_req, "idir")
    pgrp      = tde4.getCurrentPGroup()
    pnt_list  = tde4.getPointList(pgrp,0)
    cam_list  = tde4.getCameraList(1)
    cam_numb  = len(cam_list)

    if not pgrp:
        tde4.postQuestionRequester("Warning",
                                   "There is no current point group.",
                                   "Okay")

    elif cam_numb < 2:
        tde4.postQuestionRequester("Warning",
                                   "This script will run whatever cameras are selected, so please select at least two them!",
                                   "Okay")

    elif not cb_idir or not os.path.isdir(cb_idir):
        tde4.postQuestionRequester("Warning",
                                   "Please select a valid input directory!",
                                   "Okay")

    else:
        file_list = fnmatch.filter(os.listdir(cb_idir), '*.txt')
        file_list = sorted(file_list)
        file_numb = len(file_list)

        warn = 1
        if cam_numb != file_numb:
            warn = tde4.postQuestionRequester("Warning",
                                              "The number of selected cameras(%d) does not match the number of .txt files(%d)." % (cam_numb, file_numb),
                                              "Continue",
                                              "Cancel")

        if warn != 2:

            for imprt_file, cam in zip(file_list, cam_list):
                # Code stolen from import_tracks v.1.1 (And modified)

                fram_numb = tde4.getCameraNoFrames(cam)
                imag_wdth = tde4.getCameraImageWidth(cam)
                imag_hght = tde4.getCameraImageHeight(cam)

                text_file = open(cb_idir + imprt_file, "r")
                if not text_file.closed:
                    line_numb = text_file.readline()
                    line_numb = int(line_numb)
                    for i in range(line_numb):
                        pnt_name = text_file.readline()
                        pnt_name = pnt_name.strip()
                        pnt = tde4.findPointByName(pgrp, pnt_name)
                        if pnt == None:
                            pnt = tde4.createPoint(pgrp)
                        tde4.setPointName(pgrp, pnt, pnt_name)

                        pnt_colr = text_file.readline()
                        pnt_colr = int(pnt_colr)
                        tde4.setPointColor2D(pgrp, pnt, pnt_colr)

                        l = []
                        for j in range(fram_numb):
                            l.append([-1.0, -1.0])
                        n = text_file.readline()
                        n = int(n)
                        for j in range(n):
                            line = text_file.readline()
                            line = line.split()
                            l[int(line[0])-1] = [float(line[1])/imag_wdth, float(line[2])/imag_hght]
                        tde4.setPointPosition2DBlock(pgrp, pnt, cam, 1, l)
                    text_file.close()
                else:
                    tde4.postQuestionRequester("Import 2d Tracks for Multiple Cameras",
                                               "Error, couldn't open file.",
                                               "Ok")

    return

# Creates a new window unless there's an old one
try:
    req = _imc_req
except (ValueError,NameError,TypeError):
    req = tde4.createCustomRequester()
    _imc_req = req

    # Widgets
    tde4.addFileWidget(req,"idir", "Input directory: ", "*")
    tde4.addButtonWidget(req,"impt", "Import", 80, 155)
    tde4.setWidgetCallbackFunction(req, "impt", "_imcCallback")

tde4.postCustomRequesterAndContinue(req, "Import 2d tracks to the selected cameras", 500, 0)
